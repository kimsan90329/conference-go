from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    query = f"{city} {state}"
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    picture_url = response.json()["photos"][0]["src"]["original"]
    print(picture_url)
    return {"picture_url": picture_url}
    # Create a dictionary for the headers to use in the request
    # Create the URL for the request with the city and state
    # Make the request
    # Parse the JSON response
    # Return a dictionary that contains a `picture_url` key and
    #   one of the URLs for one of the pictures in the response


def get_weather_data(location):
    query = f"{location.city}, {location.state.abbreviation}, USA"
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={query}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    lat = response.json()[0]["lat"]
    lon = response.json()[0]["lon"]

    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    main_temp = response.json()["main"]["temp"]
    weather_description = response.json()["weather"][0]["description"]

    return {"main temp": main_temp, "weather description": weather_description}

    # Create the URL for the geocoding API with the city and state
    # Make the request
    # Parse the JSON response
    # Get the latitude and longitude from the response

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
